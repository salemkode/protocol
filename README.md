# Introduction

The Electrum Cash Protocol ([`@electrum-cash/protocol`](https://www.npmjs.com/package/@electrum-cash/protocol)) provides fully typed functions and events for all [Electrum Cash](https://electrum-cash-protocol.readthedocs.io/en/latest/index.html) [methods](https://electrum-cash-protocol.readthedocs.io/en/latest/protocol-methods.html).

For complete information on all typings, arguments and options available, read to the [documentation](https://electrum-cash.gitlab.io/protocol/) generated from the source code.

## Usage

### Installation

Installation is easy, just get the library from NPM:

```sh
npm install @electrum-cash/protocol
```

### Setup

Before you can make requests you first need to set up and connect to a server.

```ts
// Import the electrum protocol library.
import { initializeElectrumClient } from '@electrum-cash/protocol';

// Initialize an electrum client with following the electrum protocol.
const electrumClient = await initializeElectrumClient('Electrum Protocol Test', 'bch.imaginary.cash');
```

### Event handling

After you have initialized an electrum client and subscribe to new information, it will emit fully typed subscription events.

```ts
// Set up handler functions for electrum events.
electrumClient.on('blockchain.headers.subscribe', handleNewBlockchainHeaders);
electrumClient.on('blockchain.address.subscribe', handleAddressStatusUpdates);
electrumClient.on('blockchain.transaction.subscribe', handleTransactionStatusUpdates);
electrumClient.on('blockchain.transaction.dsproof.subscribe', handleTransactionDoublespendEvents);

// Subscribe to block headers.
await subscribeToBlockheaderUpdates(electrumClient);

// Subscribe to address status updates for a given address.
await subscribeToAddressUpdates(electrumClient, someAddress);

// Subscribe to transaction block inclusion updates for a given transaction.
await subscribeToTransactionUpdates(electrumClient, someTransactionHash);

// Subscribe to transaction doublespend updates for a given transaction.
await subscribeToDoublespendUpdates(electrumClient, someTransactionHash);
```

### Requesting data

After you have initialized an electrum client you can request data with the following functions.

#### Address

```ts
// Import address related requests.
import { fetchHistory, fetchPendingTransactions, fetchBalance, fetchUnspentTransactionOutputs } from '@electrum-cash/protocol';

// Fetch transaction history for an address.
const currentAddressHistory = await fetchHistory(electrumClient, someAddress);
console.log('history', currentAddressHistory);

// Fetch pending transactions relating to an address.
const pendingTransactions = await fetchPendingTransactions(electrumClient, someAddress);
console.log('pending transactions', pendingTransactions);

// Fetch the current spendable balance of an addess.
const currentTrustedBalance = await fetchBalance(electrumClient, someAddress);
console.log('trusted balance', currentTrustedBalance);

// Fetch a list of unspent transaction outputs for an address.
const currentUnspentOutputs = await fetchUnspentTransactionOutputs(electrumClient, someAddress);
console.log('unspent outputs', currentUnspentOutputs);
```


#### Blockchain

```ts
// Import chain related requests.
import { fetchBlockHeaderFromBlockHeight, fetchBlockHeaders, fetchBlockHeaderWithProofFromBlockHeight, fetchCurrentChainTip } from '@electrum-cash/protocol';

// Fetch the block header of a given block height.
const checkpointBlockHeader = await fetchBlockHeaderFromBlockHeight(electrumClient, someBlockHeight);
console.log('header from height', checkpointBlockHeader);

// Fetch three block headers start at a specified height.
const blockHeaders = await fetchBlockHeaders(electrumClient, electrumCheckpoint.height, 3);
console.log('list of headers', blockHeaders);

// Fetch a block header and merkle proof of the blocks inclusion in the chain.
const checkpointBlockHeaderWithProof = await fetchBlockHeaderWithProofFromBlockHeight(electrumClient, electrumCheckpoint.height, someBlockHeight);
console.log('header with proof', checkpointBlockHeaderWithProof);

// Fetch the current block height, also called the chain tip.
const currentChainTip = await fetchCurrentChainTip(electrumClient);
console.log('current chaintip', currentChainTip);
```


#### Transaction

```ts
// Import transaction related requests.
import { broadcastTransaction, fetchDoublespendProof, fetchTransaction, fetchTransactionBlockHeight, fetchTransactionProof } from '@electrum-cash/protocol';

// Broadcast a transaction to the network.
const transactionHash = await broadcastTransaction(electrumClient, someTransactionHash);
console.log('txhash', transactionHash);

// Fetch double spend proofs related to a transaction, if they exist.
const doublespendProof = await fetchDoublespendProof(electrumClient, someTransactionHash);
console.log('dsproof', doublespendProof);

// Fetch a raw transaction.
const transactionHex = await fetchTransaction(electrumClient, someTransactionHash);
console.log('fetch transaction', transactionHex);

// Fetch the block height a given transaction was included in, if possible.
const transactionBlockHeight= await fetchTransactionBlockHeight(electrumClient, someTransactionHash);
console.log('fetch transaction height', transactionBlockHeight);

// Fetch a merkle proof that a transaction was included in a specific block.
const transactionProof = await fetchTransactionProof(electrumClient, someTransactionHash, 800649);
console.log('fetch transaction proof', transactionProof);
```

## Support

If you find bugs or have problems using this library, you can file issues on the [Gitlab](https://gitlab.com/electrum-cash/protocol/issues).
