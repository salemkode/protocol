// Load the testing framework.
import { expect, test } from 'vitest';

import
{
	initializeElectrumClient,

	fetchHistory,
	fetchBalance,
	fetchPendingTransactions,
	fetchUnspentTransactionOutputs,
	subscribeToAddressUpdates,
	unsubscribeFromAddressUpdates,

	fetchBlockHeaderFromBlockHeight,
	fetchBlockHeaders,
	fetchBlockHeaderWithProofFromBlockHeight,
	fetchCurrentChainTip,
	subscribeToBlockheaderUpdates,
	unsubscribeFromBlockheaderUpdates,

	broadcastTransaction,
	fetchDoublespendProof,
	fetchTransaction,
	fetchTransactionBlockHeight,
	fetchTransactionProof,
	subscribeToDoublespendUpdates,
	subscribeToTransactionUpdates,
	unsubscribeFromDoublespendUpdates,
	unsubscribeFromTransactionUpdates,
} from '../source/index';

// Import electrum and blockchain related type information.
import type { AddressSubscribeNotification, TransactionSubscribeNotification, TransactionDsProofNotification, HeadersSubscribeNotification } from '../source/interfaces';

// Import typing for electrum clients.
import type { ElectrumClient } from '@electrum-cash/network';

// Set up some basic fixtures.
const someAddress = 'bitcoincash:qr4aadjrpu73d2wxwkxkcrt6gqxgu6a7usxfm96fst';
const someTransaction = 'c544e2795d8225fd072d2052b1fed210c5ac6d1eeb6f8eb45916160bd642f61f';
const someBlockHeight = 801283;

// Define a local-scope electrum client to use for testing.
let electrumClient: ElectrumClient;

// Set up a test for the fetchHistory request.
async function testFetchHistory(): Promise<void>
{
	const currentAddressHistory = await fetchHistory(electrumClient, someAddress);

	// Verify that the returned structure is an object.
	expect(typeof currentAddressHistory).toEqual('object');

	// Verify that the transaction hex matches expectations.
	expect(currentAddressHistory[0].height).toEqual(560682);
	expect(currentAddressHistory[0].tx_hash).toEqual('d7820227977fc05170f1c1957453709511fd702f7d608a2f48e450588ad8c5a9');
}

// Set up a test for the fetchPendingTransactions request.
async function testFetchPendingTransactions(): Promise<void>
{
	const pendingTransactions = await fetchPendingTransactions(electrumClient, someAddress);

	// Verify that the returned structure is an object.
	expect(typeof pendingTransactions).toEqual('object');
}

// Set up a test for the fetchBalance request.
async function testFetchBalance(): Promise<void>
{
	const currentTrustedBalance = await fetchBalance(electrumClient, someAddress);

	// Verify that the returned structure is a number.
	expect(typeof currentTrustedBalance).toEqual('object');
	expect(typeof currentTrustedBalance.confirmed).toEqual('number');
	expect(typeof currentTrustedBalance.unconfirmed).toEqual('number');
}

// Set up a test for the fetchUnspentTransactionOutputs request.
async function testFetchUnspentTransactionOutputs(): Promise<void>
{
	const currentUnspentOutputs = await fetchUnspentTransactionOutputs(electrumClient, someAddress);

	// Verify that the returned structure is a number.
	expect(typeof currentUnspentOutputs).toEqual('object');
}

// Set up a test for the subscribeToAddressUpdates request.
async function testSubscribeToAddressUpdates(): Promise<void>
{
	async function delayedEventTest(resolve, reject): Promise<void>
	{
		// Reject after 2 seconds as a fallback in case of errors.
		/* eslint-disable @typescript-eslint/no-implied-eval */
		const timeout = setTimeout(reject, 2000);

		// Set up an event handler.
		async function handleAddressSubscriptionUpdate(notification: AddressSubscribeNotification): Promise<void>
		{
			// Cancel the subscription
			await unsubscribeFromAddressUpdates(electrumClient, someAddress);

			// Verify that the returned structure matches expectations.
			expect(typeof notification).toEqual('object');
			expect(typeof notification.params).toEqual('object');
			expect(typeof notification.params[0]).toEqual('string');
			expect(typeof notification.params[1]).toEqual('string');

			// Clear the test timeout.
			clearTimeout(timeout);

			// Resolve the delayed test promise.
			resolve();
		}

		// Set up an event listening for the relevant event.
		electrumClient.on('blockchain.address.subscribe', handleAddressSubscriptionUpdate);

		// Subscribe for address updates in order to trigger en event.
		await subscribeToAddressUpdates(electrumClient, someAddress);
	}

	// Run the delayed event test.
	await new Promise(delayedEventTest);
}

// Set up a test for the fetchUnspentTransactionOutputs request.
async function testFetchBlockHeaderFromBlockHeight(): Promise<void>
{
	const checkpointBlockHeader = await fetchBlockHeaderFromBlockHeight(electrumClient, someBlockHeight);

	// Verify that the returned structure is a string.
	expect(typeof checkpointBlockHeader).toEqual('string');
	expect(checkpointBlockHeader).toBe('004000205a5fa32c5e42626997e59a0cb083eb74765a82ed27ce6602000000000000000062d6081347531d8060ceab67fdf357dce06b640cd5118d12fa03043574d8f2568c13ad640b77021834829b2e');
}

// Set up a test for the fetchUnspentTransactionOutputs request.
async function testFetchBlockHeaders(): Promise<void>
{
	const blockHeaders = await fetchBlockHeaders(electrumClient, 800000, 3);

	// Verify that the returned structure is a number.
	expect(typeof blockHeaders).toEqual('object');
	expect(blockHeaders[0]).toBe('0000602093ea70b294aeef69db85981bd387285824fc5af732e487010000000000000000135576e1ba9cae5509d60b4fbb9e66612f35b5ee8db264cb12612d46cc8db60b5e34a164d46b021864abbb1d');
}

// Set up a test for the fetchUnspentTransactionOutputs request.
async function testFetchBlockHeaderWithProofFromBlockHeight(): Promise<void>
{
	const checkpointBlockHeaderWithProof = await fetchBlockHeaderWithProofFromBlockHeight(electrumClient, 800000, someBlockHeight);

	// Verify that the returned structure is a number.
	expect(typeof checkpointBlockHeaderWithProof).toEqual('object');
	expect(checkpointBlockHeaderWithProof.root).toBe('7841f23c61fe4be9c0edc6232afa6ecf934466e9e5e07b5ac48d92afd135ee61');
}

// Set up a test for the fetchUnspentTransactionOutputs request.
async function testFetchCurrentChainTip(): Promise<void>
{
	const currentChainTip = await fetchCurrentChainTip(electrumClient);

	// Verify that the returned structure is a number.
	expect(typeof currentChainTip).toEqual('object');
	expect(typeof currentChainTip.height).toEqual('number');
	expect(typeof currentChainTip.hex).toEqual('string');
}

// Set up a test for the subscribeToBlockheaderUpdates request.
async function testSubscribeToBlockheaderUpdates(): Promise<void>
{
	async function delayedEventTest(resolve, reject): Promise<void>
	{
		// Reject after 2 seconds as a fallback in case of errors.
		/* eslint-disable @typescript-eslint/no-implied-eval */
		const timeout = setTimeout(reject, 2000);

		// Set up an event handler.
		async function handleBlockHeaderSubscriptionUpdate(notification: HeadersSubscribeNotification): Promise<void>
		{
			// Cancel the subscription
			await unsubscribeFromBlockheaderUpdates(electrumClient);

			// Verify that the returned structure matches expectations.
			expect(typeof notification).toEqual('object');
			expect(typeof notification.params).toEqual('object');
			expect(typeof notification.params[0].height).toEqual('number');
			expect(typeof notification.params[0].hex).toEqual('string');

			// Clear the test timeout.
			clearTimeout(timeout);

			// Resolve the delayed test promise.
			resolve();
		}

		// Set up an event listening for the relevant event.
		electrumClient.on('blockchain.headers.subscribe', handleBlockHeaderSubscriptionUpdate);

		// Subscribe for address updates in order to trigger en event.
		await subscribeToBlockheaderUpdates(electrumClient);
	}

	// Run the delayed event test.
	await new Promise(delayedEventTest);
}

// TODO: broadcastTransaction

// Set up a test for the fetchTransaction request.
async function testFetchTransaction(): Promise<void>
{
	const transactionHex = await fetchTransaction(electrumClient, someTransaction);

	// Verify that the returned structure is a number.
	expect(typeof transactionHex).toEqual('string');
	expect(transactionHex).toBe('0200000001dd2381888d1a5b7d163a8e1aa89da6702b07bc3b594d47f0e3a521864703d65f00000000fdfc0140df54ade156a5bd3579f33d9da6470c3e16c0b3124edc94e8d5bca2759d4d486ded18e430e2c85c3729533bc0e67394f5ef780b4d141c06e4910d845830f6896d108d19a7648672020075720200466e000040cda9a3badd1f0255d0e0335e18a094351c76a53023e08d60b87848acf783a60ed57d5c888c284452751460cc3c68bc0870c57fa0a0235d347e8767dfdf3c3cad10ca19a76487720200767202004a6e0000514d540104be19a76404667e9a640350520302164a031274500500e87648172102d09db08af1ff4e8453919cc866a4be427d7bfe18f2c05e5444c196fcf6fd28181976a91414096600b93c7b50c29fe25582c4906f946e74bf88ac1976a914ebdeb6430f3d16a9c6758d6c0d7a400c8e6bbee488ac002103551dd2a6e60143ec31267f910ec34ac64c2ab6c9bbdf09f7d8685f500d8eda50210394c6d87a35bcd1adc86854e0683ced98bff38d018861b2275387f1b8ac45d5045c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b6902220276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768feffffff02450b3600000000001976a914ebdeb6430f3d16a9c6758d6c0d7a400c8e6bbee488accd681a00000000001976a91414096600b93c7b50c29fe25582c4906f946e74bf88ac00000000');
}

// Set up a test for the fetchTransactionBlockHeight request.
async function testFetchTransactionBlockHeight(): Promise<void>
{
	const transactionBlockHeight = await fetchTransactionBlockHeight(electrumClient, someTransaction);

	// Verify that the returned structure is a number.
	expect(typeof transactionBlockHeight).toEqual('number');
	expect(transactionBlockHeight).toBe(800649);
}

// Set up a test for the fetchTransactionProof request.
async function testFetchTransactionProof(): Promise<void>
{
	const transactionProof = await fetchTransactionProof(electrumClient, someTransaction, 800649);

	// Verify that the returned structure is a object.
	expect(typeof transactionProof).toEqual('object');
	expect(transactionProof.block_height).toBe(800649);
	expect(transactionProof.pos).toBe(47);
}

// Set up a test for the fetchDoublespendProof request.
async function testFetchDoublespendProof(): Promise<void>
{
	const doublespendProof = await fetchDoublespendProof(electrumClient, someTransaction);

	// Verify that the returned structure is an object.
	expect(typeof doublespendProof).toEqual('object');
	expect(doublespendProof).toBe(null);
}

// Set up a test for the subscribeToTransactionUpdates request.
async function testSubscribeToTransactionUpdates(): Promise<void>
{
	async function delayedEventTest(resolve, reject): Promise<void>
	{
		// Reject after 2 seconds as a fallback in case of errors.
		/* eslint-disable @typescript-eslint/no-implied-eval */
		const timeout = setTimeout(reject, 2000);

		// Set up an event handler.
		async function handleTransactionSubscriptionUpdate(notification: TransactionSubscribeNotification): Promise<void>
		{
			// Cancel the subscription
			await unsubscribeFromTransactionUpdates(electrumClient, someTransaction);

			// Verify that the returned structure matches expectations.
			expect(typeof notification).toEqual('object');
			expect(typeof notification.params).toEqual('object');
			expect(typeof notification.params[0]).toEqual('string');
			expect(typeof notification.params[1]).toEqual('number');

			// Clear the test timeout.
			clearTimeout(timeout);

			// Resolve the delayed test promise.
			resolve();
		}

		// Set up an event listening for the relevant event.
		electrumClient.on('blockchain.transaction.subscribe', handleTransactionSubscriptionUpdate);

		// Subscribe for address updates in order to trigger en event.
		await subscribeToTransactionUpdates(electrumClient, someTransaction);
	}

	// Run the delayed event test.
	await new Promise(delayedEventTest);
}

// Set up a test for the subscribeToDoublespendUpdates request.
async function testSubscribeToDoublespendUpdates(): Promise<void>
{
	async function delayedEventTest(resolve, reject): Promise<void>
	{
		// Reject after 2 seconds as a fallback in case of errors.
		/* eslint-disable @typescript-eslint/no-implied-eval */
		const timeout = setTimeout(reject, 2000);

		// Set up an event handler.
		async function handleDoublespendSubscriptionUpdate(notification: TransactionDsProofNotification): Promise<void>
		{
			// Cancel the subscription
			await unsubscribeFromDoublespendUpdates(electrumClient, someTransaction);

			// Verify that the returned structure matches expectations.
			expect(typeof notification).toEqual('object');
			expect(typeof notification.params).toEqual('object');
			expect(notification.params[0]).toBe('c544e2795d8225fd072d2052b1fed210c5ac6d1eeb6f8eb45916160bd642f61f');

			// Clear the test timeout.
			clearTimeout(timeout);

			// Resolve the delayed test promise.
			resolve();
		}

		// Set up an event listening for the relevant event.
		electrumClient.on('blockchain.transaction.dsproof.subscribe', handleDoublespendSubscriptionUpdate);

		// Subscribe for address updates in order to trigger en event.
		await subscribeToDoublespendUpdates(electrumClient, someTransaction);
	}

	// Run the delayed event test.
	await new Promise(delayedEventTest);
}

// Set up normal tests.
async function runNormalTests(): Promise<void>
{
	test('Address: request history', testFetchHistory);
	test('Address: request mempool transactions', testFetchPendingTransactions);
	test('Address: request trusted balance', testFetchBalance);
	test('Address: request unspent transaction outputs', testFetchUnspentTransactionOutputs);
	test('Address: status update events', testSubscribeToAddressUpdates);

	test('Blockchain: request block header at height', testFetchBlockHeaderFromBlockHeight);
	test('Blockchain: request block headers from height', testFetchBlockHeaders);
	test('Blockchain: request block header with proof', testFetchBlockHeaderWithProofFromBlockHeight);
	test('Blockchain: request chain tip', testFetchCurrentChainTip);
	test('Blockchain: new header events', testSubscribeToBlockheaderUpdates);

	// TODO: broadcastTransaction
	test('Transaction: request raw transaction', testFetchTransaction);
	test('Transaction: request inclusion height', testFetchTransactionBlockHeight);
	test('Transaction: request inclusion proof', testFetchTransactionProof);
	test('Transaction: request doublespend proof', testFetchDoublespendProof);
	test('Transaction: transaction update events', testSubscribeToTransactionUpdates);
	test('Transaction: new doublespend events', testSubscribeToDoublespendUpdates);
}

async function runTests(): Promise<void>
{
	// Set up a connection with indexing servers.
	electrumClient = await initializeElectrumClient('electrum protocol integration tests', 'bch.imaginary.cash');

	// Run normal tests.
	await runNormalTests();
}

// Run all tests.
runTests();
