// Import typing for electrum clients.
import type { ElectrumClient } from '@electrum-cash/network';

// ...
import type
{
	// Primitives
	BlockHeight,
	BlockHeaderHex,

	// Block related requests.
	BlockHeaderResponse,
	BlockHeaderWithProofResponse,
	BlockHeadersResponse,

	// Header related requests.
	HeadersGetTipResponse,
} from '../interfaces';

/**
 * @module Blockchain
 * @memberof Network
 */

/**
 * Fetches the current block header for a given block height from the network.
 * @group Requests
 *
 * @param electrumClient - an Electrum Client used to connect to the network.
 * @param blockHeight    - block height to get block header for.
 *
 * @returns the block header as a hex-encoded string.
 */
export async function fetchBlockHeaderFromBlockHeight(electrumClient: ElectrumClient, blockHeight: BlockHeight): Promise<BlockHeaderHex>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the block header from the backend network servers.
	const blockHeaderHex = await electrumClient.request('blockchain.block.header', blockHeight) as BlockHeaderResponse | Error;

	// Throw an error if the request failed.
	if(blockHeaderHex instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = blockHeaderHex.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch block header from block height: ${errorMessage}`));
	}

	// Return the block header.
	return blockHeaderHex;
}

/**
 * Fetches the current block header with an inclusion proof for a given block height from the network.
 * @group Requests
 *
 * @param electrumClient   - an Electrum Client used to connect to the network.
 * @param blockHeight      - block height to get block header for.
 * @param checkpointHeight - block height of the checkpoint to use as the base of the inclusion proof.
 *
 * @returns the block header with inclusion proof.
 */
export async function fetchBlockHeaderWithProofFromBlockHeight(electrumClient: ElectrumClient, blockHeight: BlockHeight, checkpointHeight: BlockHeight): Promise<BlockHeaderWithProofResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the block header with inclusion proof from the backend network servers.
	const blockHeaderWithProof = await electrumClient.request('blockchain.block.header', blockHeight, checkpointHeight) as BlockHeaderWithProofResponse | Error;

	// Throw an error if the request failed.
	if(blockHeaderWithProof instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = blockHeaderWithProof.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch block header with proof from block height: ${errorMessage}`));
	}

	// Return the block header with the inclusion proof.
	return blockHeaderWithProof;
}

/**
 * Fetches the one or more block headers starting at a given height.
 * @group Requests
 *
 * @param electrumClient      - an Electrum Client used to connect to the network.
 * @param startingBlockHeight - block height for the first block header to request.
 * @param count               - the maximum number of block headers to request.
 *
 * @returns a list of block headers.
 */
export async function fetchBlockHeaders(electrumClient: ElectrumClient, startingBlockHeight: BlockHeight, count: number): Promise<Array<BlockHeaderHex>>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the block header with inclusion proof from the backend network servers.
	const response = await electrumClient.request('blockchain.block.headers', startingBlockHeight, count) as BlockHeadersResponse | Error;

	// Throw an error if the request failed.
	if(response instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = response.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch up to ${count} block headers starting with #${startingBlockHeight}: ${errorMessage}`));
	}

	// Extract the concatenated block headers.
	const concatenatedBlockHeaders = response.hex;

	// Initialize an empty list of block headers.
	const blockHeaders: Array<BlockHeaderHex> = [];

	// TODO: Move this to a constant somewhere.
	const blockHeaderLength = 80 * 2;

	// Extract each block header hash and push them into the list.
	for(let index = 0; index < response.count; index += 1)
	{
		blockHeaders.push(concatenatedBlockHeaders.slice(index * blockHeaderLength, (index + 1) * blockHeaderLength));
	}

	// Return the list of block headers
	return blockHeaders;
}

/**
 * Fetches the current chain tip from the network.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 *
 * @returns the current block height and block hash of the current chain tip.
 */
export async function fetchCurrentChainTip(electrumClient: ElectrumClient): Promise<HeadersGetTipResponse>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Fetch the current chain tip.
	const currentChainTip = await electrumClient.request('blockchain.headers.get_tip') as HeadersGetTipResponse | Error;

	// Throw an error if the request failed.
	if(currentChainTip instanceof Error)
	{
		// Extract the error message from the error response.
		const errorMessage = currentChainTip.message;

		// Throw an error with additional context.
		throw(new Error(`Failed to fetch the current chain tip: ${errorMessage}`));
	}

	// Return the fetched chain tip.
	return currentChainTip;
}

/**
 * Subscribes to new block headers.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 *
 * @note before calling a subscription related method, you should set up an event listener to handle the generated notifications.
 */
export async function subscribeToBlockheaderUpdates(electrumClient: ElectrumClient): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Subscribe to new block headers.
	await electrumClient.subscribe('blockchain.headers.subscribe');
}

/**
 * Unsubscribes from new block headers.
 * @group Requests
 *
 * @param electrumClient  - an Electrum Client used to connect to the network.
 */
export async function unsubscribeFromBlockheaderUpdates(electrumClient: ElectrumClient): Promise<void>
{
	// Ensure the electrum client is connected.
	await electrumClient.connect();

	// Unsubscribe from new block headers.
	await electrumClient.unsubscribe('blockchain.headers.subscribe');
}
