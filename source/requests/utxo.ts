/*
// Import utility functions.
import { generateTokenFilter } from './utilities';

// Import typing for electrum clients.
import type { ElectrumClient } from '@electrum-cash/network';

// ...
import type
{
	// Primitives
	BlockHeight,
	BlockHeaderHex,
	BlockHeaderHash,

	// UTXO related requests.

}
from '../interfaces';

/**
 * @module UTXO
 * @memberof Network
 *
 * @ignore
 */

/**
 * TODO:
 * blockchain.utxo.get_info
 */
