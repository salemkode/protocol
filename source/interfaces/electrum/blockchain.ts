/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

import type { Satoshis } from '../blockchain';

/**
 * Return the estimated transaction fee per kilobyte for a transaction to be confirmed within a certain number of blocks.
 *
 * @memberof Blockchain
 */
export type EstimateFeeRequest =
{
	/**  Must be: 'blockchain.estimatefee'. */
	method: 'blockchain.estimatefee';

	/** The number of blocks to target for confirmation. */
	number: number;
};

/**
 * The estimated transaction fee in coin units per kilobyte, as a floating point number. If the daemon does not have enough information to make an estimate, the integer -1 is returned.
 *
 * @memberof Blockchain
 */
export type EstimateFeeResponse = Satoshis;

/**
 * Return the minimum fee a low-priority transaction must pay in order to be accepted to the daemon’s memory pool.
 *
 * @memberof Blockchain
 */
export type RelayFeeRequest =
{
	/**  Must be: 'blockchain.relayfee'. */
	method: 'blockchain.relayfee';
};

/**
 * The fee in whole coin units (BTC, not satoshis for Bitcoin) as a floating point number.
 *
 * @memberof Blockchain
 */
export type RelayFeeResponse = Satoshis;
