/**
 * This file documents externally defined 3rd party interfaces,
 * so we need to disable some linting rules that cannot be enforced.
 *
 * Furthermore, the descriptions of theses interfaces are copied almost
 * verbatim from https://electrum-cash-protocol.readthedocs.io/en/latest/
 */

/* eslint-disable camelcase */

/* Import named types */
import type
{
	BlockHeight,
	BlockHeaderHex,
	BlockMerkleRoot,
	BlockMerkleProof,
} from '../blockchain';

/**
 * Return the block header at the given height.
 *
 * @memberof Blockchain.Block
 */
export type BlockHeaderRequest =
{
	/**  Must be: 'blockchain.block.header'. */
	method: 'blockchain.block.header';

	/** The height of the block, a non-negative integer. */
	height: BlockHeight;

	/** Checkpoint height, a non-negative integer. Ignored if zero, otherwise the following must hold: height <= cp_height */
	cp_height?: BlockHeight;
};

/**
 * Response typing for blockchain.block.header.
 *
 * @memberof Blockchain.Block
 */
export type BlockHeaderResponse = string;
export type BlockHeaderWithProofResponse =
{
	branch: BlockMerkleProof;
	header: BlockHeaderHex;
	root: BlockMerkleRoot;
};

/**
 * Return a concatenated chunk of block headers from the main chain.
 *
 * @memberof Blockchain.Block
 */
export type BlockHeadersRequest =
{
	/**  Must be: 'blockchain.block.headers'. */
	method: 'blockchain.block.headers';

	/** The height of the first header requested, a non-negative integer. */
	start_height: BlockHeight;

	/** The number of headers requested, a non-negative integer. */
	count: number;

	/** Checkpoint height, a non-negative integer. Ignored if zero, otherwise the following must hold: start_height + (count - 1) <= cp_height */
	cp_height?: BlockHeight;
};

/**
 * Response typing for blockchain.block.headers.
 *
 * @memberof Blockchain.Block
 */
export type BlockHeadersResponse =
{
	count: number;
	hex: BlockHeaderHex;
	max: number;
};
export type BlockHeadersWithProofResponse = BlockHeadersResponse &
{
	root: BlockMerkleRoot;
	branch: BlockMerkleProof;
};
